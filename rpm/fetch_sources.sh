#rpmspec -q perl-Library-CallNumber-LC.spec --qf "%{SOURCE}"
for f in SPECS/*.spec; do
    version="`grep ^Version: $f | awk '{ print $2 }'`"
    source="`grep ^Source0: $f | awk '{ print $2 }' | sed s/%{version}/$version/`"
    cd SOURCES; wget -c $source; cd ..
done
