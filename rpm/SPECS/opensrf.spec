Name:           opensrf
Version:        2.0.1
Release:        1%{?dist}
Summary:        OpenSRF Message Routing Network
Group:          Development/Libraries
License:        GPL2+
URL:            http://evergreen-ils.org/opensrf.php
Source0:        http://evergreen-ils.org/downloads/opensrf-%{version}.tar.gz
Source1:        ejabberd.patch
Patch0:         opensrf-apache.patch
Patch1:         opensrf-perl.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  autoconf autoconf automake check check-devel ejabberd expat-devel gcc gdbm-devel httpd httpd-devel less libgcrypt-devel libmemcached libmemcached-devel libtool libxml2-devel libxml2-python libxslt-devel make memcached mod_perl perl-Cache-Memcached perl-Class-DBI perl-Class-DBI-AbstractSearch perl-Class-DBI-SQLite perl-DateTime-Format-Builder perl-DateTime-Format-ISO8601 perl-DateTime-Format-Mail perl-DateTime-Set perl-devel perl-Error perl-File-Find-Rule perl-JSON-XS perl-libwww-perl perl-Log-Log4perl perl-Module-Build perl-Net-Jabber perl-Net-Server perl-RPC-XML perl-SQL-Abstract-Limit perl-Template-Toolkit perl-Test-Deep perl-Test-Exception perl-Test-Pod perl-Tie-IxHash perl-UNIVERSAL-require perl-Unix-Syslog perl-XML-LibXML perl-XML-LibXSLT perl-XML-Simple psmisc python-devel python-dns python-memcached python-setuptools python-simplejson readline-devel
Requires:       ejabberd expat gcc gdbm httpd less libgcrypt libmemcached libtool libxml2 libxml2-python libxslt memcached mod_perl perl-Cache-Memcached perl-Class-DBI perl-Class-DBI-AbstractSearch perl-Class-DBI-SQLite perl-DateTime-Format-Builder perl-DateTime-Format-ISO8601 perl-DateTime-Format-Mail perl-DateTime-Set perl-devel perl-Error perl-File-Find-Rule perl-JSON-XS perl-libwww-perl perl-Log-Log4perl perl-Module-Build perl-Net-Jabber perl-Net-Server perl-RPC-XML perl-SQL-Abstract-Limit perl-Template-Toolkit perl-Test-Deep perl-Test-Exception perl-Test-Pod perl-Tie-IxHash perl-UNIVERSAL-require perl-Unix-Syslog perl-XML-LibXML perl-XML-LibXSLT perl-XML-Simple psmisc python python-dns python-memcached python-setuptools python-simplejson readline


%description
Open Service Request Framework (OpenSRF, pronounced "open surf")
OpenSRF is a message routing network that offers scalability and
failover support for individual services and entire servers with
minimal development and deployment overhead.


%prep
%setup -q -n opensrf-%{version}
%patch0 -p1 -b .apache
%patch1 -p1 -b .perl


%build
./autogen.sh
./configure --prefix=/opensrf --sysconfdir=/opensrf/conf
make


%install
make DESTDIR=%{buildroot} install
mkdir -p "%{buildroot}/opensrf/var/log" "%{buildroot}/opensrf/var/run" "%{buildroot}/opensrf/var/lock"
install -Dm644 %{_sourcedir}/ejabberd.patch %{buildroot}/etc/ejabberd/opensrf.patch
install -Dm755 %{_sourcedir}/opensrf.init %{buildroot}%{_initrddir}/opensrf
mkdir -p "%{buildroot}/opensrf/var/log" "%{buildroot}/opensrf/var/run" "%{buildroot}/opensrf/var/lock"


%check


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README LICENSE.txt
/opensrf
/etc/ejabberd/opensrf.patch
%{_initrddir}/*
%{_libdir}/httpd/modules/*
%{perl_vendorlib}/*
%{_libdir}/perl5/vendor_perl/auto/OpenSRF/.packlist
%{_mandir}/man3/*



%post
/sbin/chkconfig --add opensrf
HFQDN=$(perl -MNet::Domain -e 'print Net::Domain::hostfqdn() . "\n";')
HOSTS_FILE_TAG="OPENSRF_RPM Addresses"
if ! grep -q "$HOSTS_FILE_TAG" /etc/hosts
    then 
        cp -f /etc/hosts /etc/hosts.orig
        sed -i "\$a\#$HOSTS_FILE_TAG" /etc/hosts
        sed -i "\$a\127.0.1.2\tpublic.$HFQDN\tpublic #OPENSRF_RPM" /etc/hosts
        sed -i "\$a\127.0.1.3\tprivate.$HFQDN\tprivate #OPENSRF_RPM" /etc/hosts
fi
if ! grep -q "^opensrf:" /etc/passwd 
    then
        useradd -m -s /bin/bash opensrf
fi
if ! grep -q "/opensrf/bin" /home/opensrf/.bashrc
    then
        sed -i "\$a\export PATH=/opensrf/bin:\$PATH" /home/opensrf/.bashrc
fi
patch -N -p1 /etc/ejabberd/ejabberd.cfg < /etc/ejabberd/opensrf.patch
###/etc/init.d/ejabberd restart
###sleep 5
ejabberdctl --node ejabberd@`hostname -s` register router private.localhost password
ejabberdctl --node ejabberd@`hostname -s` register opensrf private.localhost password
ejabberdctl --node ejabberd@`hostname -s` register router public.localhost password
ejabberdctl --node ejabberd@`hostname -s` register opensrf public.localhost password
ldconfig
su - opensrf -c "cp /opensrf/conf/srfsh.xml.example /home/opensrf/.srfsh.xml"
cp /opensrf/conf/opensrf.xml.example /opensrf/conf/opensrf.xml
cp /opensrf/conf/opensrf_core.xml.example /opensrf/conf/opensrf_core.xml
cp /opensrf/conf/srfsh.xml.example /opensrf/conf/srfsh.xml
chown -R opensrf:opensrf /opensrf


%changelog
* Wed Jun 29 2011 Ben Webb <bjwebb67@googlemail.com> - 2.0.0-1
- Initial build
