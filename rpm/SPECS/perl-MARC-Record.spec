Name:           perl-MARC-Record
Version:        2.0.3
Release:        1%{?dist}
Summary:        Object-oriented abstraction of MARC record handling

Group:          Development/Libraries
License:        GPL+ or Artistic
URL:            http://search.cpan.org/dist/MARC-Record/
Source0:        http://search.cpan.org/CPAN/authors/id/G/GM/GMCHARLT/MARC-Record-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  perl(Test::Pod)
BuildRequires:  perl(Test::Pod::Coverage)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
The MARC::* series of modules create a simple object-oriented
abstraction of MARC record handling.


%prep
%setup -q -n MARC-Record-%{version}
chmod -c 644 lib/MARC/*.pm


%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} ';'
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null ';'
chmod -R u+w $RPM_BUILD_ROOT/*


%check
make test


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc Changes README
%{_bindir}/*
%{perl_vendorlib}/MARC/
%{_mandir}/man1/*.1*
%{_mandir}/man3/*.3pm*


%changelog
* Wed Jul 27 2011 Ben Webb <bjwebb67@googlemail.com> - 2.0.3-11
- Update to 2.0.3

* Mon Jun 20 2011 Marcela MaÅ¡lÃ¡ÅˆovÃ¡ <mmaslano@redhat.com> - 2.0.0-10
- Perl mass rebuild

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.0-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Dec 20 2010 Marcela Maslanova <mmaslano@redhat.com> - 2.0.0-8
- 661697 rebuild for fixing problems with vendorach/lib

* Mon May 03 2010 Marcela Maslanova <mmaslano@redhat.com> - 2.0.0-7
- Mass rebuild with perl-5.12.0

* Mon Dec  7 2009 Stepan Kasal <skasal@redhat.com> - 2.0.0-6
- rebuild against perl 5.10.1

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Nov 20 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 2.0.0-3
- fix source url

* Thu Mar 06 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 2.0.0-2
Rebuild for new perl

* Tue Jan 30 2007 Jose Pedro Oliveira <jpo at di.uminho.pt> - 2.0.0-1
- Update to 2.0.0.

* Wed Sep 13 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.38-1
- First build.

