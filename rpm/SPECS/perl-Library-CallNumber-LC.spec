Name:           perl-Library-CallNumber-LC
Version:        0.10
Release:        1%{?dist}
Summary:        Utility functions to deal with Library of Congress Call Numbers

Group:          Development/Libraries
License:        GPL+ or Artistic
URL:            http://search.cpan.org/dist/Library-CallNumber-LC-0.10/lib/Library/CallNumber/LC.pm
Source0:        http://search.cpan.org/CPAN/authors/id/D/DU/DUEBERB/Library-CallNumber-LC-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Module::Build)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))


%description
Library::CallNumber::LC is mostly designed to do call number normalization, with the following goals:
The normalized call numbers are comparable with each other, for proper sorting
The normalized call number is a short as possible, so left-anchored wildcard searches are possible (e.g., searching on "A11*" should give you all the A11 call numbers)
A range defined by start_of_range and end_of_range should be correct, assuming that the string given for the end of the range is, in fact, a left prefix.


%prep
%setup -q -n Library-CallNumber-LC-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf $RPM_BUILD_ROOT
./Build install destdir=$RPM_BUILD_ROOT create_packlist=0
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;


%check
./Build test


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*


%changelog
* Wed Jun 29 2011 Ben Webb <bjwebb67@googlemail.com> - 0.10-1
- Initial build
