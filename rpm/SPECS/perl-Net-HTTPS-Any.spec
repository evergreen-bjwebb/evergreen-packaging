Name:           perl-Net-HTTPS-Any
Version:        0.10
Release:        1%{?dist}
Summary:        Simple HTTPS class using whichever underlying SSL module is available
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Net-HTTPS-Any/
Source0:        http://www.cpan.org/modules/by-module/Net/Net-HTTPS-Any-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(Tie::IxHash)
BuildRequires:  perl(URI::Escape)
BuildRequires:  perl(Net::SSLeay)
BuildRequires:  perl(Crypt::SSLeay)
Requires:       perl(Test::More)
Requires:       perl(Tie::IxHash)
Requires:       perl(URI::Escape)
Requires:       perl(Net::SSLeay)
Requires:       perl(Crypt::SSLeay)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
This is a simple wrapper around either of the two available SSL modules. It
offers a unified API for sending GET and POST requests over HTTPS and
receiving responses.

%prep
%setup -q -n Net-HTTPS-Any-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;

%{_fixperms} $RPM_BUILD_ROOT/*

%check
# Disabled due to build services that don't have full network access
#make test

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Thu Jun 30 2011 Ben Webb <bjwebb67@googlemail.com> 0.10-1
- Initial build based on specfile autogenerated by cpanspec 1.78.
