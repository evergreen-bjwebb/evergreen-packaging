Name:           evergreen-ils
Version:        2.1+git20110807
Release:        1%{?dist}
Summary:         Evergreen Library Management Sofware.

Group:          Development/Libraries
License:        GPL2+
URL:            http://evergreen-ils.org/opensrf.php
Source0:        http://xn--gce.com/evergreen-git/evergreen-ils_%{version}.orig.tar.gz
Patch0:         evergreen-ils-git-support_scripts.patch
Patch1:         evergreen-ils-apache.patch
Patch2:         evergreen-ils-git-perl.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  opensrf httpd-devel
BuildRequires:  aspell aspell-en libdbi libdbi-dbd-pgsql libdbi-devel libssh2-devel libmemcached-devel libyaz libyaz-devel mod_ssl ncurses-devel ncurses-libs perl-parent perl-Business-CreditCard perl-Business-ISBN perl-Business-ISBN-Data perl-DBD-Pg perl-Email-Send perl-Email-Simple perl-GDGraph3d perl-Net-IP perl-Net-SSH2 perl-OLE-Storage_Lite perl-Spreadsheet-WriteExcel perl-Text-Aspell perl-Text-CSV perl-Text-CSV_XS perl-XML-Writer postgresql-devel readline-devel tcp_wrappers-devel wget yaz
BuildRequires:  perl(Business::EDI) perl(Business::OnlinePayment) perl(Business::OnlinePayment::AuthorizeNet) perl(Business::OnlinePayment::HTTPS) perl(Class::DBI::Frozen::301) perl(CQL::Parser) perl(Library::CallNumber::LC) perl(List::Util) perl(LWP::UserAgent) perl(MARC::Charset) perl(MARC::File::XML) perl(Net::HTTPS::Any) perl(Net::Z3950::Simple2ZOOM) perl(Net::Z3950::SimpleServer) perl(Net::Z3950::ZOOM) perl(SRU) perl(URI::Escape) perl(UUID::Tiny) perl(JavaScript::SpiderMonkey)
BuildRequires:  chrpath
Requires:  aspell aspell-en libdbi libdbi-dbd-pgsql libssh2 libyaz libyaz mod_ssl ncurses ncurses-libs perl-parent perl-Business-CreditCard perl-Business-ISBN perl-Business-ISBN-Data perl-DBD-Pg perl-Email-Send perl-Email-Simple perl-GDGraph3d perl-Net-IP perl-Net-SSH2 perl-OLE-Storage_Lite perl-Spreadsheet-WriteExcel perl-Text-Aspell perl-Text-CSV perl-Text-CSV_XS perl-XML-Writer postgresql-server readline tcp_wrappers wget yaz
Requires:  perl(Business::EDI) perl(Business::OnlinePayment) perl(Business::OnlinePayment::AuthorizeNet) perl(Business::OnlinePayment::HTTPS) perl(Class::DBI::Frozen::301) perl(CQL::Parser) perl(Library::CallNumber::LC) perl(List::Util) perl(LWP::UserAgent) perl(MARC::Charset) perl(MARC::File::XML) perl(Net::HTTPS::Any) perl(Net::Z3950::Simple2ZOOM) perl(Net::Z3950::SimpleServer) perl(Net::Z3950::ZOOM) perl(SRU) perl(URI::Escape) perl(UUID::Tiny) perl(JavaScript::SpiderMonkey)
Requires:  postgresql-contrib postgresql-plperl
Requires:  opensrf
Requires:  perl(MARC::Record) >= 2.0.1
Requires:  xulrunner-1.9

%description
Evergreen is open-source, consortial-quality library software to help library
users find library materials and to help libraries manage, catalog, and
circulate those materials. 


# prevent anything matching from being scanned for requires
%define filter_requires_in(P) %{expand: \
%global __filter_req_cmd %{?__filter_req_cmd} %{__grep} -v %{-P} '%*' | \
}
# actually set up the filtering bits
%define filter_setup %{expand: \
%global _use_internal_dependency_generator 0 \
%global __deploop() while read FILE; do /usr/lib/rpm/rpmdeps -%{1} ${FILE}; done | /bin/sort -u \
%global __find_provides /bin/sh -c "%{?__filter_prov_cmd} %{__deploop P} %{?__filter_from_prov}" \
%global __find_requires /bin/sh -c "%{?__filter_req_cmd}  %{__deploop R} %{?__filter_from_req}" \
}
%filter_requires_in /
%filter_setup


%prep
%setup -q -n evergreen-ils_%{version}
%patch0 -p1 -b .support_scripts
%patch1 -p1 -b .apache
%patch2 -p1 -b .perl

%build
./autogen.sh
./configure --with-opensrf-headers=/opensrf/include/ \
    --with-opensrf-libs=/opensrf/lib/
make


%install
make STAFF_CLIENT_BUILD_ID=rel_2_0_7 DESTDIR=%{buildroot} install
mkdir -p "%{buildroot}/openils/bin"
mkdir -p "%{buildroot}/openils/share/doc/examples/"
cp -r %{_builddir}/evergreen-ils_%{version}/Open-ILS/src/support-scripts/* "%{buildroot}/openils/bin/"
cp -r %{_builddir}/evergreen-ils_%{version}/Open-ILS/src/sql "%{buildroot}/openils/share/doc/examples/"
mkdir -p %{buildroot}/etc/httpd/conf.d/
install %{_builddir}/evergreen-ils_%{version}/Open-ILS/examples/apache/eg.conf       %{buildroot}/etc/httpd/conf.d/
install %{_builddir}/evergreen-ils_%{version}/Open-ILS/examples/apache/eg_vhost.conf %{buildroot}/etc/httpd/
install %{_builddir}/evergreen-ils_%{version}/Open-ILS/examples/apache/startup.pl    %{buildroot}/etc/httpd/
ln -s "/opensrf/conf" "%{buildroot}/openils/conf"
mkdir -p "%{buildroot}/openils/var/run" "%{buildroot}/openils/var/lock" "%{buildroot}/openils/var/log"
mkdir -p %{buildroot}/opensrf/lib/
for f in `ls %{buildroot}/openils/lib`
    do ln -s /openils/lib/$f %{buildroot}/opensrf/lib/
done
mkdir -p %{buildroot}/etc/ld.so.conf.d/
echo "/usr/lib/dbd/" > %{buildroot}/etc/ld.so.conf.d/eg.conf

mkdir -p "%{buildroot}/openils/"
cp -r "%{_builddir}/evergreen-ils_%{version}/Open-ILS/xul/staff_client/build" "%{buildroot}/openils/client"


%check


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README LICENSE.txt
/openils
/opensrf
/etc/httpd
%{_libdir}/httpd/modules/*
/etc/ld.so.conf.d/*
%{perl_vendorlib}/*
%{_mandir}/man3/*
%{_libdir}/perl5/vendor_perl/auto/OpenILS/.packlist


%post
ldconfig
install /openils/etc/opensrf.xml.example /opensrf/conf/opensrf.xml
ln -s /opensrf/conf/opensrf.xml /openils/etc/
install /openils/etc/opensrf_core.xml.example /opensrf/conf/opensrf_core.xml
ln -s /opensrf/conf/opensrf_core.xml /openils/etc/
install /openils/etc/oils_web.xml.example /opensrf/conf/oils_web.xml
ln -s /opensrf/conf/oils_web.xml /openils/etc/
# Set up the evergreen database
if su - postgres -c "createdb -T template0 --lc-ctype=C --lc-collate=C -E UNICODE evergreen" > /dev/null 2>&1
then
    su - postgres -c "createlang plperl evergreen"
    su - postgres -c "createlang plperlu evergreen"
    su - postgres -c "createlang plpgsql evergreen"
    su - postgres -c "psql -f /usr/share/pgsql/contrib/tablefunc.sql evergreen"
    su - postgres -c "psql -f /usr/share/pgsql/contrib/tsearch2.sql evergreen"
    su - postgres -c "psql -f /usr/share/pgsql/contrib/pgxml.sql evergreen"
    su - postgres -c "psql -f /usr/share/pgsql/contrib/hstore.sql evergreen"
fi
# Add evergreen database user
if [ `su - postgres -c "psql -A -t -c \"select count(*) from pg_roles where rolname='evergreen'\""` = "0" ]
then  
    tfile=`mktemp`
cat << EOF > $tfile
CREATE ROLE evergreen PASSWORD 'fixmepls' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;
EOF
    chown postgres.postgres $tfile
    su - postgres -c "psql -f $tfile"
    rm $tfile
fi
# Populate database
###cd /openils/bin/ && perl eg_db_config.pl --update-config --service all --create-schema \
###	--create-offline --hostname localhost --port 5432 --user evergreen --password fixmepls \
###	--database evergreen --admin-user admin --admin-pass open-ils
# Apache and bashrc
echo "LoadModule osrf_json_gateway_module /usr/lib/httpd/modules/osrf_json_gateway.so
LoadModule osrf_http_translator_module /usr/lib/httpd/modules/osrf_http_translator.so
LoadModule xmlent_module      /usr/lib/httpd/modules/mod_xmlent.so
LoadModule idlchunk_module    /usr/lib/httpd/modules/mod_idlchunk.so" >> /etc/httpd/conf/httpd.conf
if [ ! -d "/etc/httpd/ssl" ]; then
    mkdir /etc/httpd/ssl
    cd /etc/httpd/ssl
    openssl req -new -x509 -days 365 -nodes -out server.crt -keyout server.key -subj "/CN=localhost"
fi
if grep -q "export PERL5LIB=/openils/lib/perl5:\$PERL5LIB" /home/opensrf/.bashrc
    then echo "export PERL5LIB=/openils/lib/perl5:\$PERL5LIB" | sudo -u opensrf tee -a /home/opensrf/.bashrc
fi
chown -R opensrf:opensrf /openils
chown -R opensrf:opensrf /opensrf

%changelog
* Wed Jun 29 2011 Ben Webb <bjwebb67@googlemail.com> - 2.0.7-1
- Initial build
