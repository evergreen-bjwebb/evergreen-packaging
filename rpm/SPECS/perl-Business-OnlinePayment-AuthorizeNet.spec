Name:           perl-Business-OnlinePayment-AuthorizeNet
Version:        3.21
Release:        1%{?dist}
Summary:        AuthorizeNet backend for Business::OnlinePayment
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Business-OnlinePayment-AuthorizeNet/
Source0:        http://www.cpan.org/modules/by-module/Business/Business-OnlinePayment-AuthorizeNet-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl(Business::OnlinePayment) >= 3
BuildRequires:  perl(Business::OnlinePayment::HTTPS)
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Net::SSLeay)
BuildRequires:  perl(Test::More) >= 0.42
BuildRequires:  perl(Text::CSV_XS)
BuildRequires:  perl(Tie::IxHash)
BuildRequires:  perl(XML::Simple)
BuildRequires:  perl(XML::Writer)
Requires:       perl(Business::OnlinePayment) >= 3
Requires:       perl(Business::OnlinePayment::HTTPS)
Requires:       perl(Net::SSLeay)
Requires:       perl(Test::More) >= 0.42
Requires:       perl(Text::CSV_XS)
Requires:       perl(Tie::IxHash)
Requires:       perl(XML::Simple)
Requires:       perl(XML::Writer)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
For detailed information see Business::OnlinePayment.

%prep
%setup -q -n Business-OnlinePayment-AuthorizeNet-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;

%{_fixperms} $RPM_BUILD_ROOT/*

%check
#make test

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Thu Jun 30 2011 Ben Webb <bjwebb67@googlemail.com> 3.21-1
- Initial build based on a specfile autogenerated by cpanspec 1.78.
