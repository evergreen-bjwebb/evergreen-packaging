Name:           xulrunner-1.9
Version:        1.9.3
Release:        1%{?dist}
Summary:        Mozilla's application framework.

Group:          Development/Libraries
License:        GPL2+
URL:            https://developer.mozilla.org/en/XULRunner_1.9.2_Release_Notes
Source0:        http://releases.mozilla.org/pub/mozilla.org/xulrunner/releases/1.9.2.17/runtimes/xulrunner-1.9.2.17.en-US.linux-i686.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: libnotify alsa-lib dbus-glib nss nss-softokn-freebl
Requires: gtk2 hunspell libevent sqlite libIDL libXrender libXt startup-notification

%description


%filter_from_requires %{_libdir}/libnotify.so.1
%filter_setup

%prep
%setup -q -n xulrunner

%build



%install
mkdir -p "%{buildroot}/opt" "%{buildroot}/usr/bin" "%{buildroot}/%{_libdir}"
cp -r . "%{buildroot}/opt/xulrunner"
ln -s "/opt/xulrunner/xulrunner" "%{buildroot}/usr/bin/xulrunner-1.9"
ln -s "%{_libdir}/libnotify.so.4" "%{buildroot}%{_libdir}/libnotify.so.1"


%check


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
/opt/xulrunner
/usr/bin/*
%{_libdir}/*


%post

%changelog
* Wed Jun 29 2011 Ben Webb <bjwebb67@googlemail.com> - 2.0.7-1
- Initial build
