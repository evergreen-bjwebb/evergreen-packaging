Name:           perl-SRU
Version:        0.99
Release:        1%{?dist}
Summary:        Search and Retrieval by URL
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/SRU/
Source0:        http://search.cpan.org/CPAN/authors/id/B/BR/BRICAS/SRU-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 1:5.8.0
BuildRequires:  perl(Class::Accessor)
BuildRequires:  perl(CQL::Parser) >= 0.3
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Test::Exception)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(URI)
BuildRequires:  perl(XML::LibXML)
BuildRequires:  perl(XML::Simple)
BuildRequires:  perl(CGI)
Requires:       perl(Class::Accessor)
Requires:       perl(CQL::Parser) >= 0.3
Requires:       perl(URI)
Requires:       perl(XML::LibXML)
Requires:       perl(XML::Simple)
Requires:       perl(CGI)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
The SRU package provides a framework for working with the Search and
Retrieval by URL (SRU) protocol developed by the Library of Congress. SRU
defines a web service for searching databases containing metadata and
objects. SRU often goes under the name SRW which is a SOAP version of the
protocol. You can think of SRU as a RESTful version of SRW, since all the
requests are simple URLs instead of XML documents being sent via some sort
of transport layer.

%prep
%setup -q -n SRU-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;

%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Thu Jun 30 2011 Ben Webb <bjwebb67@googlemail.com> 0.99-1
- Specfile autogenerated by cpanspec 1.78.
