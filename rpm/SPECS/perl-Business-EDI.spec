Name:           perl-Business-EDI
Version:        0.05
Release:        1%{?dist}
Summary:        Top level class for generating U.N. EDI interchange objects and subobjects. 
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Business-EDI/
Source0:        http://search.cpan.org/CPAN/authors/id/J/JO/JOEATZ/Business-EDI-%{version}.tgz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(CGI)  
BuildRequires:  perl(Carp)
BuildRequires:  perl(Data::Dumper)
BuildRequires:  perl(Exporter)
BuildRequires:  perl(Exporter::Easy)
BuildRequires:  perl(File::Find::Rule)
BuildRequires:  perl(File::Spec)
BuildRequires:  perl(JSON::XS)
BuildRequires:  perl(UNIVERSAL::require)
BuildRequires:  perl(List::MoreUtils)
BuildRequires:  perl(Test::More) >= 0.47
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
The focus of functionality is to provide object based access to EDI messages and
subelements. At present, the EDI input processed by Business::EDI objects is
JSON from the edi4r ruby library, and there is no EDI output beyond the perl
objects themselves.

%prep
%setup -q -n atz-Business-EDI-327e01f

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;

%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Thu Jun 30 2011 Ben Webb <bjwebb67@googlemail.com> 0.05-1
- Initial Build
