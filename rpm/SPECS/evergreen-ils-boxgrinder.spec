Name:           evergreen-ils-boxgrinder
Version:        0.1
Release:        1%{?dist}
Summary:        Custom scripts.

Group:          Development/Libraries
License:        GPL2+
URL:            http://evergreen-ils.org/
Source0:        http://xn--gce.com/evergreen/evergreen-ils-boxgrinder_%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       patch

%description
Extra scripts for the Evergreen boxgrinder images.

# prevent anything matching from being scanned for requires
%define filter_requires_in(P) %{expand: \
%global __filter_req_cmd %{?__filter_req_cmd} %{__grep} -v %{-P} '%*' | \
}
# actually set up the filtering bits
%define filter_setup %{expand: \
%global _use_internal_dependency_generator 0 \
%global __deploop() while read FILE; do /usr/lib/rpm/rpmdeps -%{1} ${FILE}; done | /bin/sort -u \
%global __find_provides /bin/sh -c "%{?__filter_prov_cmd} %{__deploop P} %{?__filter_from_prov}" \
%global __find_requires /bin/sh -c "%{?__filter_req_cmd}  %{__deploop R} %{?__filter_from_req}" \
}
%filter_requires_in /
%filter_setup


%prep
%setup -q -n evergreen-ils-boxgrinder_%{version}

%build

%install
install -Dm755 %{_builddir}/evergreen-ils-boxgrinder_%{version}/evergreen-ils-boxgrinder.init %{buildroot}%{_initrddir}/evergreen-ils-boxgrinder
rm %{_builddir}/evergreen-ils-boxgrinder_%{version}/evergreen-ils-boxgrinder.init
mkdir -p %{buildroot}/openils
cp -r %{_builddir}/evergreen-ils-boxgrinder_%{version} %{buildroot}/openils/boxgrinder
%check


%clean
rm -rf %{buildroot}


%files
%{_initrddir}/*
/openils/boxgrinder/*


%post
/sbin/chkconfig --add evergreen-ils-boxgrinder
install -m755 /openils/boxgrinder/hostname /bin/hostname
hostname
echo "opensrf:opensrf" | chpasswd
#echo "RUN_FIRSTBOOT=no" > /etc/sysconfig/firstboot


%changelog
* Wed Jun 29 2011 Ben Webb <bjwebb67@googlemail.com> - 2.0.0-5
- Initial build
