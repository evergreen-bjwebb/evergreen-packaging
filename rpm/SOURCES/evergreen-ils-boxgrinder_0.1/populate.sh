basedir=`pwd`

#cd $basedir/gen_fake_users
#./gen_fake_users.pl last_names.txt male_names.txt female_names.txt abbr_suffix.txt wordlist.txt zip5.csv > users.sql
#cat users.sql | su - postgres -c "psql evergreen"


cd $basedir
#sed -i "s/\${prefix}/\/openils/g" import/*
for f in datasets/*.marc datasets/*.mrc; do
    PERL5LIB=/openils/lib/perl5/ \
        perl import/marc2bre.pl --db_user evergreen --db_host localhost --db_pw fixmepls $f \
        | perl import/pg_loader.pl -or bre -or mrd -or mfr -or mtfe -or mafe -or msfe -or mkfe -or msefe -a mrd -a mfr -a mtfe -a mafe -a msfe -a mkfe -a msefe \
        | su - postgres -c "psql evergreen"
done

####PERL5LIB=/openils/lib/perl5/ perl import/marc2bre.pl --marctype XML --start 1 --idfield 901 --idsubfield a datasets/serials_marc21.xml | perl import/pg_loader.pl -or bre -or mrd -or mfr -or mtfe -or mafe -or msfe -or mkfe -or msefe -a mrd -a mfr -a mtfe -a mafe -a msfe -a mkfe -a msefe | su - postgres -c "psql evergreen"
#PERL5LIB=/openils/lib/perl5/ perl import/marc2bre.pl --db_user evergreen --db_host localhost --db_pw fixmepls OL.20100104.26 | perl import/pg_loader.pl -or bre -or mrd -or mfr -or mtfe -or mafe -or msfe -or mkfe -or msefe -a mrd -a mfr -a mtfe -a mafe -a msfe -a mkfe -a msefe | su - postgres -c "psql evergreen"
su - postgres -c "psql evergreen -f $basedir/import/quick_metarecord_map.sql"
#PERL5LIB=/openils/lib/perl5/ perl import/marc2sre.pl --marctype XML --libmap datasets/serials_lib.map datasets/serials_mfhd.xml | perl import/pg_loader.pl -or sre > mfhd21.sql
#su - postgres -c "psql -f $basedir/mfhd21.sql evergreen"


cd $basedir
python create_holdings.py > holdings.sql
cat holdings.sql | su - postgres -c "psql evergreen"
