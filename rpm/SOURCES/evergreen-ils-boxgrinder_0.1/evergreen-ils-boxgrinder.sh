#!/bin/bash

if [ ! -f /openils/boxgrinder/postpostinstalled ]; then
    service postgresql initdb
    # Allow password database authentication
    if ! grep -q evergreen /var/lib/pgsql/data/pg_hba.conf ; then
        sed -i "s|127\.0\.0\.1/32            ident|127\.0\.0\.1/32            md5|" /var/lib/pgsql/data/pg_hba.conf
        sed -i "s|::1/128                 ident|::1/128                 md5|" /var/lib/pgsql/data/pg_hba.conf
    fi
    service postgresql start
    
    # Fix apache config
    sed -i "s/KeepAlive Off/KeepAlive On/;s/KeepAliveTimeout 5/KeepAliveTimeout 1/;s/User apache/User opensrf/" /etc/httpd/conf/httpd.conf
    
    ## Putting this here because /etc/hosts is rewritten after installation.
    HOSTS_FILE_TAG="OPENSRF_RPM Addresses"
    if ! grep -q "$HOSTS_FILE_TAG" /etc/hosts
        then 
            cp -f /etc/hosts /etc/hosts.orig
            sed -i "\$a\#$HOSTS_FILE_TAG" /etc/hosts
            sed -i "\$a\127.0.1.2\tpublic.localhost\tpublic #OPENSRF_RPM" /etc/hosts
            sed -i "\$a\127.0.1.3\tprivate.localhost\tprivate #OPENSRF_RPM" /etc/hosts
            sed -Ei 's/\.localdomain6? localhost6?//' /etc/hosts
    fi
    /etc/init.d/ejabberd restart
    while ! ejabberdctl status >/dev/null ; do
        cnt=`expr $cnt + 1`
        if [ $cnt -ge 60 ] ; then
            echo "Can't register Ejabberd users."
            break
        fi
        sleep 1
    done
    ejabberdctl --node ejabberd@`hostname -s` register router private.localhost password
    ejabberdctl --node ejabberd@`hostname -s` register opensrf private.localhost password
    ejabberdctl --node ejabberd@`hostname -s` register router public.localhost password
    ejabberdctl --node ejabberd@`hostname -s` register opensrf public.localhost password
    
    # bashrc
    if ! grep -q "export PERL5LIB=/openils/lib/perl5:\$PERL5LIB" /home/opensrf/.bashrc
        then echo "export PERL5LIB=/openils/lib/perl5:\$PERL5LIB" | sudo -u opensrf tee -a /home/opensrf/.bashrc
    fi
    if ! grep -q "export PATH=/openils/bin:\$PATH" /home/opensrf/.bashrc
        then echo "export PATH=/openils/bin:\$PATH" | sudo -u opensrf tee -a /home/opensrf/.bashrc
    fi

    echo "Running database setup script"
    
    until `su - postgres -c "psql -l" > /dev/null`; do sleep 1s; echo -n .; done
    echo;
    
    # Set up the evergreen database
    if su - postgres -c "createdb -T template0 --lc-ctype=C --lc-collate=C -E UNICODE evergreen" > /dev/null 2>&1
    then
        su - postgres -c "createlang plperl evergreen"
        su - postgres -c "createlang plperlu evergreen"
        su - postgres -c "createlang plpgsql evergreen"
        su - postgres -c "psql -f /usr/share/pgsql/contrib/tablefunc.sql evergreen"
        su - postgres -c "psql -f /usr/share/pgsql/contrib/tsearch2.sql evergreen"
        su - postgres -c "psql -f /usr/share/pgsql/contrib/pgxml.sql evergreen"
        su - postgres -c "psql -f /usr/share/pgsql/contrib/hstore.sql evergreen"
    fi
    # Add evergreen database user
    if [ `su - postgres -c "psql -A -t -c \"select count(*) from pg_roles where rolname='evergreen'\""` = "0" ]
    then  
        tfile=`mktemp`
        cat << EOF > $tfile
CREATE ROLE evergreen PASSWORD 'fixmepls' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;
EOF
        chown postgres.postgres $tfile
        su - postgres -c "psql -f $tfile"
        rm $tfile
    fi
    
    # Populate database
    cd /openils/bin/ && echo | perl eg_db_config.pl --update-config --service all --create-schema \
            --create-offline --hostname localhost --port 5432 --user evergreen --password fixmepls \
            --database evergreen --admin-user admin --admin-pass open-ils
    
    /etc/init.d/memcached start
    /etc/init.d/opensrf start
    
    # Autogen
    su - opensrf -c "autogen.sh"
    
    touch /openils/boxgrinder/postpostinstalled
else
    /etc/init.d/postgresql start
    /etc/init.d/ejabberd start
    /etc/init.d/memcached start
    /etc/init.d/opensrf start
fi
