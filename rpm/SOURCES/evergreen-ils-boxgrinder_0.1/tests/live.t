# wget "http://192.168.0.6:81/test.t" -O test.t; perl test.t
use Test::More qw(no_plan);
use Digest::MD5  qw(md5_hex);
use Data::Dumper;
use LWP::Simple;
use URI::Escape;
use JSON::XS;

my $admin_pass = "open-ils";
my $only_testdata = 1; # Use to enable/disable tests that assume the catalogue
                       # contains the test data and only the test data

BEGIN {
    use_ok('OpenSRF::AppSession');
    use_ok('OpenSRF::System');
}


sub opensrf_request {
    if ($method eq "direct") {
        my $service = shift(@_);
        my $session = OpenSRF::AppSession->create($service);
        my $result = $session->request(@_);
        $response = $result->gather();
        $session->disconnect();
        return $response;
    }
    else {
        my $service = uri_escape(shift(@_));
        my $method = uri_escape(shift(@_));
        my @params = @_;
        my $paramstring = "";
        foreach (@params) {
            $coder = JSON::XS->new->ascii->pretty->allow_nonref;
            $paramstring .= "&param=" . uri_escape($coder->encode($_));
        }
        $result = decode_json get("http://localhost/osrf-gateway-v1?method=$method$paramstring&service=$service");
        if ($result->{"status"} != 200)  { die("osrf-gateway encountered an error"); }
        return $result->{"payload"}->[0];
    }
}

ok ( OpenSRF::System->bootstrap_client(config_file => '/openils/conf/opensrf_core.xml') );

foreach (("direct", "http")) {
    $method = $_;
    
    is ( opensrf_request("opensrf.math", "add", 2, 2), 4,           "Test opensrf.math");
    
    like ( my $auth_seed = opensrf_request("open-ils.auth",
            "open-ils.auth.authenticate.init",
            "admin"),                           qr/[a-f0-9]{32}/,   "Init auth");
    
    my @auth = {
        username=>"admin",
        password=>md5_hex($auth_seed . md5_hex($admin_pass))
        };
    ok ( my $auth_response = opensrf_request("open-ils.auth",
            "open-ils.auth.authenticate.complete",
            @auth),                                                 "Complete auth");
    is ( $auth_response->{"desc"}, "Success",                       "Verify that login succeeded");
    is ( $auth_response->{"textcode"}, "SUCCESS",                   "Verify that login succeeded");
    like ( my $auth_token =
        $auth_response->{"payload"}{"authtoken"}, qr/[a-f0-9]{32}/, "Check that authtoken is returned");
    
    
    
    $word = "tset";
    $correct_word = "test";
    ok ( my $spell = opensrf_request("open-ils.search",
                        "open-ils.search.spellcheck", $word),       "Check a simple spelling");
    is ( $spell->[0]->{"word"}, $word );
    is ( $spell->[0]->{"suggestions"}->[0], $correct_word );
    
    
    $query = { searches=>{ title=>{term=>"linux"} } };
    ok ( my $search1 = opensrf_request("open-ils.search",
            "open-ils.search.biblio.multiclass", $query),           "Do an OPAC-style search");
    print Dumper($search1);
    ok ( my $search2 = opensrf_request("open-ils.search",
            "open-ils.search.biblio.multiclass.query",
            {}, "title:linux"),                                     "Do an OPAC-style search");
    #print Dumper($search2);
    if ($only_testdata) {
        is( $search1->{"count"}, 2,                                 "Check count matches the test data" );
        is( $search2->{"count"}, 2,                                 "Check count matches the test data" );
        $ids = $search1->{"ids"};
        foreach $id ( @$ids ) {
            ok ( $fieldmap_record = opensrf_request("open-ils.fielder", "opensrf.open-ils.system.fieldmapper", "Fieldmapper::biblio::record_entry") );
            ok ( $marc_pos = $fieldmap_record->{"Fieldmapper::biblio::record_entry"}->{"fields"}->{"marc"}->{"position"} );
            print int($id->[0]);
            $rrr = opensrf_request("open-ils.supercat", "open-ils.supercat.record.object.retrieve", (int($id->[0])));
            #$rrr = opensrf_request("open-ils.search", "open-ils.search.asset.copy.retrieve", int($id->[0]));
            #{"__p"}->
            if ($method eq "direct") {
                $fields = $rrr->[0];
            }
            else {
                $fields = $rrr->[0]->{"__p"};
            }
            print $marc_pos;
            print Dumper($fields->[$marc_pos]);
            last;
        }
    }
    else {
        is( $search1->{"count"}, $search2->{"count"},               "Check search methods match" );
    }
}