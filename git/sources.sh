. git.sh

if [ $OPENSRF_NEW ]; then
  cd $OPENSRF_PACKAGE
  dch -v $OPENSRF_VERSION-1 Automated git head release.
  debuild -S -sa
fi
if [ $EVERGREEN_NEW ]; then
 cd $basedir
 cd $EVERGREEN_PACKAGE
 dch -v $EVERGREEN_VERSION-1 Automated git head release.
 debuild -S -sa
fi

cd $basedir

