OPENSRF_VERSION=2.0.1+git`date +%Y%m%d`
OPENSRF_PACKAGE=opensrf_$OPENSRF_VERSION
EVERGREEN_VERSION=2.1+git`date +%Y%m%d`
EVERGREEN_PACKAGE=evergreen-ils_$EVERGREEN_VERSION

basedir=`pwd`
if [ -d OpenSRF ] ; then
  cd OpenSRF && git pull origin
else
  git clone git://git.evergreen-ils.org/OpenSRF.git
  cd OpenSRF
fi
mv ../opensrf.log ../opensrf.log.1
git log > ../opensrf.log
if ! diff ../opensrf.log ../opensrf.log.1 >/dev/null ; then
  rm -rf ../opensrf_*
  git archive --prefix $OPENSRF_PACKAGE/ --format tar HEAD | gzip > ../$OPENSRF_PACKAGE.orig.tar.gz
  OPENSRF_NEW=1
fi
cd $basedir

if [ -d Evergreen ] ; then
  cd Evergreen && git pull origin
else
  git clone git://git.evergreen-ils.org/Evergreen.git
  cd Evergreen
fi
mv ../evergreen-ils.log ../evergreen-ils.log.1
git log > ../evergreen-ils.log
if ! diff ../evergreen-ils.log ../evergreen-ils.log.1 >/dev/null ; then
  rm -rf  ../evergreen-ils_*
  git archive --prefix $EVERGREEN_PACKAGE/ --format tar HEAD | gzip > ../$EVERGREEN_PACKAGE.orig.tar.gz
  EVERGREEN_NEW=1
fi
cd $basedir

mkdir $OPENSRF_PACKAGE $EVERGREEN_PACKAGE
for pname in $OPENSRF_PACKAGE $EVERGREEN_PACKAGE
do
  name=`echo $pname | sed s/_.*//`
  if ! diff $name.log $name.log.1 >/dev/null ; then
    tar --strip-components=1 -C $pname -xvf $pname.orig.tar.gz
  fi
done
cp -r ../debian/opensrf_2.0.1/debian $OPENSRF_PACKAGE
sed -i "s|zlib1g-dev|zlib1g-dev,\n python-nose, python-cjson, python-memcache, python-pyxmpp, python-simplejson|" $OPENSRF_PACKAGE/debian/control


cp -r ../debian/evergreen-ils_2.1~rc2/debian $EVERGREEN_PACKAGE
rm -r $EVERGREEN_PACKAGE/debian/patches
cp -r evergreen-ils-patches $EVERGREEN_PACKAGE/debian/patches

