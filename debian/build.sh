apt-get install build-essential locales
# apt-get remove libdbi-dev
PACKAGES="libclass-dbi-frozen-301-perl spidermonkey opensrf spidermonkey-perl evergreen-ils evergreen-ils-live"
#PACKAGES="evergreen-ils"
for f in $PACKAGES; do
    #apt-get -y build-dep $f
    pushd ${f}_*/
    dpkg-buildpackage
    popd
    dpkg -i ${f}_*.deb
done
