#!/usr/bin/perl
#   gen_fake_users.pl : simple script to generate SQL for adding fake patrons to an Evergreen database
#   Copyright (C) 2008 Equinox Software Inc.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
my $n = 10; # number of patrons to generate

###############################################
# examples
my @profiles = ( 2, 12, 13, 14 );
my @ident_types = ( 1, 3 );
my @home_ou = ( 4, 5, 6, 7, 8, 9 );
###############################################

my @last_names = ();
my @name_suffixes = ( 'Jr', 'Sr', 'III', 'II' );
my @male_names = ();
my @female_names = ();
my @street_suffixes = ();
my @nouns = ();
my @adjectives = ();
my @zips = (); my %zip = {};

sub gen_sql {
    my ($profile, $ident_type, $usrname, $home_ou, $family_name, $passwd, $first_given_name, $second_given_name, $expire_date, $dob, $suffix) = 
       (shift,    shift,       shift,    shift,    shift,        shift,   shift,             shift,              shift,        shift,shift);
    my ($country, $within_city_limits, $post_code, $street1, $valid, $state, $city, $street2, $county) =
       (shift,    shift,               shift,      shift,    shift,  shift,  shift, shift,    shift);
    my $barcode = shift;

    print "BEGIN;\n"; 
    print qq^INSERT INTO actor.usr (profile, ident_type, usrname, home_ou, family_name, passwd, first_given_name, second_given_name, expire_date, dob, suffix) VALUES ($profile, $ident_type, '$usrname', $home_ou, '$family_name', '$passwd', '$first_given_name', '$second_given_name', '$expire_date', '$dob', '$suffix');\n^;
    print qq^INSERT INTO actor.usr_address (country, within_city_limits, post_code, street1, valid, state, city, street2, county, usr) VALUES ('$country', '$within_city_limits', '$post_code', '$street1', '$valid', '$state', '$city', '$street2', '$county', CURRVAL('actor.usr_id_seq'));\n^;
    print qq^INSERT INTO actor.card (barcode, usr) VALUES ('$barcode', CURRVAL('actor.usr_id_seq'));\n^;
    print qq^UPDATE actor.usr SET card = CURRVAL('actor.card_id_seq'), billing_address = CURRVAL('actor.usr_address_id_seq'), credit_forward_balance = '0', mailing_address = CURRVAL('actor.usr_address_id_seq') WHERE id=CURRVAL('actor.usr_id_seq');\n^;
    print "COMMIT;\n"; 

}

if ( ! $ARGV[0] && ! $ARGV[1] && ! $ARGV[2] && ! $ARGV[3] && ! $ARGV[4] && ! $ARGV[5] ) {
    print "File 1 is last names\nFile 2 is male (or female) first names\nFile 3 is female (or male) first names\nFile 4 is street suffixes\nFile 5 is a wordlist (word,part of speech)\nFile 6 is zip codes (zip, city, state, lattitude, longitude, county).\n\nThis script doesn't query your database for sane org unit id's, etc., so you should modify the source and change the \@profiles, \@ident_types, and \@home_ou variables.\n\nExample usage:\n\n./gen_fake_users.pl last_names.txt male_names.txt female_names.txt abbr_suffix.txt wordlist.txt zip5.csv\n\n";
    exit;
}
open FILE, "$ARGV[0]"; while (my $name = <FILE>) { if ($name =~ /(\S+)/) { push @last_names, uc(substr($1,0,1)) . lc(substr($1,1)); } }; close FILE;
open FILE, "$ARGV[1]"; while (my $name = <FILE>) { if ($name =~ /(\S+)/) { push @male_names, uc(substr($1,0,1)) . lc(substr($1,1)); } }; close FILE;
open FILE, "$ARGV[2]"; while (my $name = <FILE>) { if ($name =~ /(\S+)/) { push @female_names, uc(substr($1,0,1)) . lc(substr($1,1)); } }; close FILE;
open FILE, "$ARGV[3]"; while (my $name = <FILE>) { if ($name =~ /(\S+)/) { push @street_suffixes, uc(substr($1,0,1)) . lc(substr($1,1)); } }; close FILE;
open FILE, "$ARGV[4]"; while (my $line = <FILE>) { 
    chomp $line;
    my ($word,$part) = split(/,/,$line);
    if ($part eq "n") { push @nouns, uc(substr($word,0,1)) . lc(substr($word,1)); }
    if ($part eq "adj") { push @adjectives, uc(substr($word,0,1)) . lc(substr($word,1)); }
}; close FILE;
open FILE, "$ARGV[5]"; while (my $line = <FILE>) {
    chomp $line;
    my ($zc,$city,$state,$lat,$lon,$county) = split(/,/,$line);
    push @zips, $zc;
    $zip{ $zc } = [ uc(substr($city,0,1)) . lc(substr($city,1)), $state, uc(substr($county,0,1)) . lc(substr($county,1))  ];
}; close FILE;

my %seen_barcode = ();
my %seen_usrname = ();
foreach my $x ( 1..$n) {
    my $gender = rand(100);
    my $exp_year = 2006 + int(rand(10));
    my $exp_month = 1 + int(rand(12));
    my $exp_day = 1 + int(rand(28));
    my $dob_year = 1960 + int(rand(40));
    my $dob_month = 1 + int(rand(12));
    my $dob_day = 1 + int(rand(28));
    my $street_number = int(rand(10000)) + 1;
    my $street = "$street_number " . ( rand(100) > 33 ? $adjectives[ int(rand( scalar(@adjectives) )) ] . " " : '' ) 
        . $nouns[ int(rand( scalar(@nouns) )) ] . " " . $street_suffixes[ int(rand( scalar(@street_suffixes) )) ];
    my $zc = $zips[ int(rand( scalar(@zips) )) ];
    my $barcode;
    while(true) {
        $barcode = sprintf("%06i",int(rand(100000))+300000);
        if (! defined $seen_barcode{$barcode} ) {
            $seen_barcode{$barcode} = 1;
            last;
        }
    }
    my $usrname;
    while(true) {
        my $usrname_suffix = int(rand(1000));
        $usrname = rand(100) > 50 ? $barcode : $nouns[ int(rand( scalar(@nouns) )) ] . $usrname_suffix;
        if (! defined $seen_usrname{$usrname} ) {
            $seen_usrname{$usrname} = 1;
            last;
        }
    }

    gen_sql(
        $profiles[ int(rand( scalar(@profiles) )) ], # profile
        $ident_types[ int(rand( scalar(@ident_types) )) ], # ident_type
        $usrname, # usrname
        $home_ou[ int(rand( scalar(@home_ou) )) ], # home_ou
        $last_names[ int(rand( scalar(@last_names) )) ], # family_name
        'demo123', # passwd
        $gender > 50 ? $male_names[ int(rand( scalar(@male_names) )) ] : $female_names[ int(rand( scalar(@female_names) )) ], # first_given_name
        rand(100) > 25 ? ($gender > 50 ? $male_names[ int(rand( scalar(@male_names) )) ] : $female_names[ int(rand( scalar(@female_names) )) ]) : '', # second_given_name
        sprintf("%04i-%02i-%02i",$exp_year,$exp_month,$exp_day), # expire_date
        sprintf("%04i-%02i-%02i",$dob_year,$dob_month,$dob_day), # dob
        rand(100) > 75 ? $name_suffixes[ int(rand( scalar(@name_suffixes) )) ] : '', # suffix
        "USA", # country
        rand(100) > 50 ? 't' : 'f', # within_city_limits
        $zc, # post_code
        $street, # street1
        rand(100) > 75 ? 't' : 'f', # valid
        $zip{$zc}[1], # state
        $zip{$zc}[0], # city
        '', # street 2
        rand(100) > 25 ? $zip{$zc}[2] : '', # county
        $barcode, # barcode
    );
}
