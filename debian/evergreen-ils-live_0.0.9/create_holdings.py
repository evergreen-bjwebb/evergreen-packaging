#!/usr/bin/python
# -------------------------------------------------------------------------
# create_holdings.py : simple script to generate SQL for 
# adding auto-generated volumes and copies to an Evergreen database
# Copyright (C) 2008 Equinox Software Inc.
# Bill Erickson <erickson@esilibrary.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------


# -------------------------------------------------------------------------
# Generates random volume and copy information for test purposes.  This
# script outputs a series of SQL statements suitable for loading into an 
# Evergreen Postgres instance.
#
# Usage:
# $ ./create_holdings.py > holding.sql
# $ psql <Evergree database connection info> < holdings.sql
# -------------------------------------------------------------------------
import sys, random, string


# -------------------------------------------------------------------------
# Edit these settings
# -------------------------------------------------------------------------
''' 
ID of the user who is creating the data
everdemo=# select id from actor.usr where usrname='admin';
'''
creator_user = 1

''' How many volumes do we want to create?  '''
num_volumes = 5000

''' How many copies do we want to create?  '''
num_copies = 5000

'''
List of orgs to use as volume owning_lib's and copy circ_lib's
evergreen=# select aou.id, aou.shortname from actor.org_unit as aou, 
    actor.org_unit_type as aout where aou.ou_type = aout.id and aout.can_have_vols;
'''
org_ids = (4, 5, 6, 7, 8, 9)
# -------------------------------------------------------------------------


chars=string.letters + string.digits
def generate_string(size):
    s = ''
    for i in range(size):
        idx = random.randint(0,61)
        s += chars[idx:idx+1].upper()
    return s

def generate_nums(size):
    s = ''
    for i in range(size):
        idx = random.randint(0,9)
        s += string.digits[idx:idx+1]
    return s

def generate_vol_label():
    s = generate_string(8)
    s += '-'
    s += generate_string(4)
    return s

print 'BEGIN;\n'
for i in range(num_volumes):
    print "INSERT INTO asset.call_number (creator, editor, record, owning_lib, label)\n\tvalues " + \
        "(%d, %d, (SELECT id FROM biblio.record_entry ORDER BY RANDOM() LIMIT 1), %d, '%s');" % (
            creator_user, creator_user, org_ids[random.randint(0,len(org_ids)-1)], generate_vol_label())

for i in range(num_copies):
    print "INSERT INTO asset.copy (circ_lib, creator, call_number, editor, loan_duration, fine_level, barcode)\n\tvalues " + \
        "(%d, %d, (SELECT id FROM asset.call_number ORDER BY RANDOM() LIMIT 1), %d, 2, 2, '%s');" % (
            org_ids[0], creator_user, creator_user, generate_nums(12))
    
    ''' Now update the circ_lib to match the owning_lib of the volume '''

    print "UPDATE asset.copy SET circ_lib = (SELECT owning_lib FROM asset.call_number cn, " + \
        "asset.copy cp WHERE cn.id = cp.call_number AND cp.id = CURRVAL('asset.copy_id_seq')) " + \
            "WHERE id = CURRVAL('asset.copy_id_seq');"

print 'COMMIT;\n'
	

