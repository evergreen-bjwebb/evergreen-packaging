sources=(
    http://search.cpan.org/CPAN/authors/id/B/BO/BOBTFISH/Class-DBI-Frozen-301-3.0.1.tar.gz \
    http://search.cpan.org/CPAN/authors/id/T/TB/TBUSCH/JavaScript-SpiderMonkey-0.20.tar.gz \
    ftp://ftp.mozilla.org/pub/mozilla.org/js/js-1.7.0.tar.gz \
    http://evergreen-ils.org/downloads/opensrf-2.0.1.tar.gz \
    http://evergreen-ils.org/downloads/previews/Evergreen-ILS-2.1-RC2.tar.gz
)

pnames=(
    libclass-dbi-frozen-301-perl_3.0.1 \
    spidermonkey-perl_0.20 \
    spidermonkey_1.7.0 \
    opensrf_2.0.1 \
    evergreen-ils_2.1~rc2
)

for i in `seq 0 4`
do
    if [ `ls ${pnames[i]} | wc -l` -eq 1 ]; then
        wget -c ${sources[i]} -O ${pnames[i]}.orig.tar.gz
        tar --strip-components=1 -C ${pnames[i]} -xvf ${pnames[i]}.orig.tar.gz
    fi
done
