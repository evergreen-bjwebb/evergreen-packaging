#!/bin/bash
set -e
set +x
OPENSRF_PREFIX="/usr"
. /usr/share/debconf/confmodule
HFQDN=$(perl -MNet::Domain -e 'print Net::Domain::hostfqdn() . "\n";')
if [ -z "$HFQDN" ]; then HFQDN=localhost; fi
DEBUG()
{
	if [ $_DEBUG > 0 ]; then  $@;fi
}

modify_hosts_file() 
{
HOSTS_FILE_TAG="OPENSRF_DEB Addresses"
if ! grep -q "$HOSTS_FILE_TAG" /etc/hosts
	then 
		cp -f /etc/hosts /etc/hosts.orig
		DEBUG echo "Adding entries to /etc/hosts..."
		sed -i "2i\#$HOSTS_FILE_TAG" /etc/hosts
		sed -i "3i\127.0.1.2\tpublic.$HFQDN\tpublic #OPENSRF_DEB" /etc/hosts
		sed -i "4i\127.0.1.3\tprivate.$HFQDN\tprivate #OPENSRF_DEB" /etc/hosts
		sed -i '5i\ ' /etc/hosts
	else
		DEBUG echo "Hosts file has already been modified!"
		
fi
} #modify_hosts_file()

add_opensrf_user() {
if ! grep -q "^opensrf:" /etc/passwd 
	then
		DEBUG echo "Opensrf user does not exist! Creating..."
		useradd -m -s /bin/bash opensrf

		db_get opensrf/user_password || true
        if [ -n "$RET" ]; then
		    PASS=$RET
		    tfile=`mktemp`
		    if [ ! -f "$tfile" ]; then
			    echo "ERROR Creating temp file!"
		            return 1
		    fi
		
		    DEBUG echo "creating tmp pw file as $tfile"
cat << EOF > $tfile
opensrf:$PASS
EOF
    DEBUG echo "about to change"
		    cat $tfile | chpasswd	
    DEBUG echo "about to rm"
		    rm $tfile
		    DEBUG echo "Set user opensrf's password"
	    fi
	else
		DEBUG echo "Opensrf user already exists, so not doing anything!"
		

fi  

} #add_opensrf_user()

#EJABBERD SECTION
reconfigure_ejabberd()
{
if grep -q "^{hosts, \[\"localhost\"]}" /etc/ejabberd/ejabberd.cfg 
	then
		DEBUG echo "Stock ejabberd detecting. Stopping and modifying config!"
		invoke-rc.d ejabberd stop
		cp /etc/ejabberd/ejabberd.cfg /etc/ejabberd/ejabberd.cfg.orig
		sed -i "s/^{hosts, \[\"localhost\"\]}/{hosts, [\"localhost\", \"private.$HFQDN\", \"public.$HFQDN\"]}/g" /etc/ejabberd/ejabberd.cfg
		sed -i "s/max_user_sessions,\ \[{10,/max_user_sessions,\ \[{10000,/g" /etc/ejabberd/ejabberd.cfg
		sed -i 's/{max_stanza_size, 65536}/{max_stanza_size, 2000000}/g' /etc/ejabberd/ejabberd.cfg
		sed -i 's/{max_stanza_size, 131072}/{max_stanza_size, 2000000}/g' /etc/ejabberd/ejabberd.cfg
		sed -i 's/{shaper, normal, {maxrate, 1000}}/{shaper, normal, {maxrate, 500000}}/g' /etc/ejabberd/ejabberd.cfg
		sed -i 's/{shaper, fast, {maxrate, 50000}}/{shaper, fast, {maxrate, 500000}}/g' /etc/ejabberd/ejabberd.cfg
		sed -i 's/{mod_offline/%%{mod_offline/g' /etc/ejabberd/ejabberd.cfg
		chown ejabberd:ejabberd /etc/ejabberd/ejabberd.cfg
		invoke-rc.d ejabberd start
	else
		DEBUG echo "Ejabberd has already been modified (or atleast the hosts line has). No Changes made."
fi
} #reconfigure_ejabberd()
add_ejabber_user()
{
		USER=$1
		HOST=$2
		PASSWD=$3
                if ! status=$(ejabberdctl register "$USER" "$HOST" "$PASSWD") ; then
                    if echo $status | grep -q "already registered" ; then
                       DEBUG echo "User \"$USER@$HOST\" is already registered. Password IS NOT changed."
                    else
                        echo "Can't register user \"$USER@$HOST\"."
                    fi
                else
                    DEBUG echo "User \"$USER@$HOST\" is registered successfully."
                fi


}

register_ejabberd_users()
{
    db_get opensrf/ejabber_password
    if [ -n "$RET" ]; then
        EJABBER_PASS=$RET
        #if [ -n "$USER" -a -n "$PASSWD" ]; then
            DEBUG echo -n "Waiting for ejabberd to register admin user"

            if ejabberdctl status >/dev/null || test $? = 1 ; then
                # Ejabberd is starting ($? = 1) or running ($? = 0) already.
                cnt=0
                flag=1
                while ! ejabberdctl status >/dev/null ; do
                    DEBUG echo -n "."
                    cnt=`expr $cnt + 1`
                    if [ $cnt -ge 60 ] ; then
                        echo
                        echo "Can't register Ejabberd users."
                        echo -n "Ejabberd is taking too long to start!"
                        flag=0
                        break
                    fi
                    sleep 1
                done

                echo
                if [ $flag -eq 1 ] ; then
	                    add_ejabber_user router private.$HFQDN $EJABBER_PASS
                            add_ejabber_user opensrf private.$HFQDN $EJABBER_PASS
                            add_ejabber_user router public.$HFQDN $EJABBER_PASS
                            add_ejabber_user opensrf public.$HFQDN $EJABBER_PASS
                fi
            else
                echo
                echo "Can't register Ejabberd users!"
                echo "Ejabberd server is not started."
            fi
        #fi
    fi
} #register_ejabberd_users()


#END EJABBERD SECTION


fix_osrf_perms()
{
chown -R opensrf:opensrf /var/log/opensrf
}


configure_opensrf_xml()
{
    db_get opensrf/ejabber_password
    if [ -n "$RET" ]; then
        EJABBERPASS=$RET
        [ ! -f /etc/opensrf/opensrf.xml ] && cp /etc/opensrf/opensrf.xml.example /etc/opensrf/opensrf.xml
        [ ! -f /etc/opensrf/opensrf_core.xml ] && cp /etc/opensrf/opensrf_core.xml.example /etc/opensrf/opensrf_core.xml
        [ ! -f /etc/opensrf/srfsh.xml ] && cp /etc/opensrf/srfsh.xml.example /etc/opensrf/srfsh.xml
        sed -i "s|d>password|d>$RET|g" /etc/opensrf/*.xml
        mkdir -p /home/opensrf
        mv /etc/opensrf/srfsh.xml /home/opensrf/.srfsh.xml && \
        chown opensrf /home/opensrf/.srfsh.xml && chmod 600 /home/opensrf/.srfsh.xml
        sed -i "s/<localhost>/<$HFQDN>/g" /etc/opensrf/opensrf.xml
        sed -i "s/<\/localhost>/<\/$HFQDN>/g" /etc/opensrf/opensrf.xml
        sed -i "s/\.localhost</\.$HFQDN</g" /etc/opensrf/opensrf_core.xml
        sed -i "s/\.localhost</\.$HFQDN</g" /etc/opensrf/opensrf.xml
        sed -i "s/\.localhost</\.$HFQDN</g" /home/opensrf/.srfsh.xml
        sed -i "s|/var/log|/var/log/opensrf|" /etc/opensrf/*.xml /home/opensrf/.srfsh.xml
    fi


}

modify_hosts_file
add_opensrf_user
reconfigure_ejabberd
register_ejabberd_users
fix_osrf_perms
configure_opensrf_xml

#DEBHELPER#

db_stop
exit 0

