#!/usr/bin/env perl

@files = <perl-*>; 
foreach $file (@files) {
    open(MYINPUTFILE, "<" . $file . "/PKGBUILD");
    @m = grep(/^pkgver=/, <MYINPUTFILE>);
    my($version) = $m[0];
    chomp($version);
    $version =~ s/^pkgver=//;
    
    open(MYINPUTFILE, "<" . $file . "/PKGBUILD");
    @m = grep(/^source=/, <MYINPUTFILE>);
    my($source) = $m[0];
    chomp($source);
    $source =~ s/^source=\(//;
    $source =~ s/[ \)].*$//;
    $source =~ m/([A-Z]\/[A-Z]{2}\/[A-Z]+\/.*)\$pkgver/;
    $blob = $1;
    open(MYINPUTFILE, "<cpan/02packages.details.txt");
    @m = grep(/$blob/, <MYINPUTFILE>);
    $m[0] =~ m/$blob(.*).(tar.gz|tgz)/;
    $cpan_version = $1;    
    #$source =~ s/\$pkgver/$version/;
    if ($version != $cpan_version) {
        print $file . " " . $version . " " . $cpan_version . "\n";
    }
}

