#!/usr/bin/perl
use Expect;


system('sed -i "s/\[localhost\]:22001 ssh-rsa .*//" ~/.ssh/known_hosts');

my $exp;
$connected = 0;
while (!$connected) {
    $exp = Expect->spawn('ssh -p 22001 localhost -l root')
        or die "Cannot spawn ssh: $!\n";
    $exp->expect(undef,
        [ 'Are you sure you want to continue connecting.' => sub { $connected=1; $exp->send("yes\r") } ],
        [ qr/Connection (refused|closed)/ => sub { undef($exp); sleep(5); } ]
        );
}
$exp->expect(10, [ 'password:' => sub { $exp->send("boxgrinder\r"); } ] );

$exp->expect(10, [ '\[root@localhost' => sub{
        $exp->send("cd /openils/boxgrinder\r");
    } ] );
$exp->expect(10, [ '\[root@localhost boxgrinder\]#' => sub {
        $exp->send("if [ ! -f postpostinstalled ]; then echo -n \"Waiting for postinstall setup \"; fi; \ while [ ! -f postpostinstalled ]; do sleep 1s; echo -n .; done; echo; echo \"Postinstall setup complete.\";\r");
    } ] );
$exp->expect(undef, [ '\[root@localhost boxgrinder\]#' => sub { $exp->send("./populate.sh &> populate.log\r"); } ] );
$exp->expect(undef, [ '\[root@localhost boxgrinder\]#' => sub { $exp->send("cd tests; ./test.sh\r"); } ] );
$exp->expect(undef, [ '\[root@localhost tests\]' => sub { $exp->send("exit\r"); } ] );
